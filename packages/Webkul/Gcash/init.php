

<?php

// gash integration test file
const PUBLIC_KEY = "pk_test_DFBA8ut9VsNvkVGjeLX1egNx";
const SECRET_KEY = "sk_test_MTMRQ5wQ4T82VCtG7Tc14THf";
$response = '';
$info = '';
$error = '';
$task = '';

if(isset($_GET['task'])){
	$task = $_GET['task'];
	switch ($task) {
		case 'value':
			if(isset($_POST['inputURL'])){
				createWebHook($_POST['inputURL']);
			}
			break;
		case 'retrieveWebHook':
			retrieveWebHook();
			break;
	}
}

//step 3 is to listen to webhook 
function createWebHook($inputURL){
	global $response;
	global $info;
	global $error;
	$ch = curl_init();
	$url = 'https://api.paymongo.com/v1/webhooks';
	$postData = ['data' => 
		[
			'attributes' => [
				'url' => $inputURL,
				'events' => ['source.chargeable']
			]
		]
	];
	$headers = [
		'Content-Type: application/json'
	];
	$postData = json_encode($postData);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_USERPWD,'sk_test_MTMRQ5wQ4T82VCtG7Tc14THf' . ':' . '' );
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$postData);
	$output = curl_exec($ch);
	$connectionInfo = curl_getinfo($ch);
	$error = var_export(curl_error($ch), true);
	curl_close($ch);
	$response = var_export($output,true);
	$info = var_export($connectionInfo, true);
}


function retrieveWebHook()
{
	global $response;
	global $info;
	global $error;
	$ch = curl_init();
	$url = 'https://api.paymongo.com/webhooks/hook_CuPcRMFtdpw3mSnVPtVPqKhG';
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_USERPWD,'sk_test_MTMRQ5wQ4T82VCtG7Tc14THf' . ':' . '' );
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	$connectionInfo = curl_getinfo($ch);
	$error = var_export(curl_error($ch), true);
	curl_close($ch);
	$response = var_export($output,true);
	$info = var_export($connectionInfo, true);
}
if(isset($_POST['inputURL'])){
	echo "<h3>Output</h3><hr>";
	echo "<pre>" . $response . "</pre>";
	echo "<br><br>";
	echo "<h3>Connection Info</h3><hr>";
	echo "<pre>" . $info . "</pre>";
	echo "<h3>Curl Error</h3><hr>";
	echo "<pre>" . $error . "</pre>";
} else {
	?>
		<form method='POST'>
		<input type='text' name='inputURL'>
		<input type='submit'>
		</form>
	<?php
}
echo "<h3>Output</h3><hr>";
		echo "<pre>" . $response . "</pre>";
		echo "<br><br>";
		echo "<h3>Connection Info</h3><hr>";
		echo "<pre>" . $info . "</pre>";
		echo "<h3>Curl Error</h3><hr>";
		echo "<pre>" . $error . "</pre>";


//EOF
<?php

namespace Webkul\Paypal\Payment;

class Standard extends Paypal
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code  = 'paypal_standard';

    /**
     * Line items fields mapping
     *
     * @var array
     */
    protected $itemFieldsFormat = [
        'id'       => 'item_number_%d',
        'name'     => 'item_name_%d',
        'quantity' => 'quantity_%d',
        'price'    => 'amount_%d',
    ];

    /**
     * Return paypal redirect url
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        return route('paypal.standard.redirect');
    }

    /**
     * Return form field array
     *
     * @return array
     */
    public function getFormFields()
    {
        $cart = $this->getCart();

        $fields = [
            'business'        => $this->getConfigData('business_account'),
            'invoice'         => $cart->id,
            'currency_code'   => $cart->cart_currency_code,
            'paymentaction'   => 'sale',
            'return'          => route('paypal.standard.success'),
            'cancel_return'   => route('paypal.standard.cancel'),
            'notify_url'      => route('paypal.standard.ipn'),
            'charset'         => 'utf-8',
            'item_name'       => core()->getCurrentChannel()->name,
            'amount'          => $cart->sub_total,
            'tax'             => $cart->tax_total,
            'shipping'        => $cart->selected_shipping_rate ? $cart->selected_shipping_rate->price : 0,
            'discount_amount' => $cart->discount_amount,
        ];

        if ($this->getIsLineItemsEnabled()) {
            $fields = array_merge($fields, array(
                'cmd'    => '_cart',
                'upload' => 1,
            ));

            $this->addLineItemsFields($fields);

            if ($cart->selected_shipping_rate)
                $this->addShippingAsLineItems($fields, $cart->items()->count() + 1);

            if (isset($fields['tax'])) {
                $fields['tax_cart'] = $fields['tax'];
            }

            if (isset($fields['discount_amount'])) {
                $fields['discount_amount_cart'] = $fields['discount_amount'];
            }
        } else {
            $fields = array_merge($fields, array(
                'cmd'           => '_ext-enter',
                'redirect_cmd'  => '_xclick',
            ));
        }

        $this->addAddressFields($fields);

        return $fields;
    }

    /**
     * Add shipping as item
     *
     * @param  array  $fields
     * @param  int    $i
     * @return void
     */
    protected function addShippingAsLineItems(&$fields, $i)
    {
        $cart = $this->getCart();

        $fields[sprintf('item_number_%d', $i)] = $cart->selected_shipping_rate->carrier_title;
        $fields[sprintf('item_name_%d', $i)] = 'Shipping';
        $fields[sprintf('quantity_%d', $i)] = 1;
        $fields[sprintf('amount_%d', $i)] = $cart->selected_shipping_rate->price;
    }

    public function createGcashResource(){
        $cart = $this->getCart();
        ////////////////////////Ask for payment Gcash//////////////////////////////
        $url = "https://api.paymongo.com/v1/sources";
        $ch = curl_init();
        $postData = ['data' => 
            [
                'attributes' => [
                    'type' => 'gcash',
                    'amount' => (intval($cart->base_grand_total) + 10000), // int32
                    'currency' => 'PHP',
                    'redirect' => [
                        'success' => route('paypal.standard.success'),
                        'failed' => route('paypal.standard.cancel'),
                    ]
                ]
            ]
        ];
        $headers = [
            'Content-Type: application/json'
        ];
        //pk_test_PYDhCQS7FDaKwHeEAeGWpG7D
        //sk_test_YLWgUwMae4pnpKPsUdcUj2pv
        $postData = json_encode($postData);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_USERPWD,'pk_test_PYDhCQS7FDaKwHeEAeGWpG7D' . ':' . '' );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        //get only json
        $startPos = strpos($output,'{"');
        $lenght = strlen($output);
        $result = json_decode(substr($output,$startPos,$lenght),true);
        $checkoutUrl = $result['data']['attributes']['redirect']['checkout_url'];
        //////////////////////////////Gash End////////////////////////////////////
        \Debugbar::info(intval($cart->base_grand_total));
        return $checkoutUrl;
    }
}